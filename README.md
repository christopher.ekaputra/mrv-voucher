### Endpoint

| Method | Endpoint                                                         | Description                   |
| ------ | ---------------------------------------------------------------- | ----------------------------- |
| POST   | [/api/register](#get-apiregister)                                | register new user             |
| POST   | [/api/login](#post-apilogin)                                     | login user (get token)        |
| GET    | [/api/user/:userId](#get-apiuseruserid)                          | get detail of user            |
|        |                                                                  |                               |
| POST   | [/api/counter](#post-apicounter)                                 | entry new paket data          |
| GET    | [/api/counter](#get-apicounter)                                  | get all list of paket data    |
| PUT    | [/api/counter/:listPaketId](#put-apicounterlistpaketid)          | update list paket data        |
| POST   | [/api/counter/buy](#post-apicounterbuy)                          | user buy paket data           |
|        |                                                                  |                               |
| GET    | [/api/wallet/:walletId](#get-apiwalletwalletid)                  | Get wallet detail             |
| PUT    | [/api/wallet/:walletId](#put-apiwalletwalletid)                  | Add (update) saldo to wallet  |
|        |                                                                  |                               |
| GET    | [/api/paket-data/:paketDataId](#get-apipaket-datapaketdataid)    | Get detail of user paket data |
| DELETE | [/api/paket-data/:paketDataId](#delete-apipaket-datapaketdataid) | Reset user paket data         |

### GET /api/register

#### Req

```json
{
  "email": "farhan@gmail.com",
  "username": "farhan",
  "age": 10,
  "password": "password"
}
```

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": null
}
```

### POST /api/login

#### Req

```json
{
  "email": "farhan@gmail.com",
  "password": "password"
}
```

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": {
    "id": 1,
    "email": "farhan@gmail.com",
    "username": "farhan",
    "age": 10,
    "created": "2023-09-02T00:00:00Z",
    "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJmYXJoYW4iLCJlbWFpbCI6ImZhcmhhbkBnbWFpbC5jb20iLCJleHAiOjE2OTM2NTU0NDh9.4dU34VRELigmfA00W3D2W78TlgmAm9WrQxZReQpnFDf_lZoJJ8PO9pGMj2eH3WTfjjB72f6K3iSWTqD-Ps6d2Q"
  }
}
```

### GET /api/user/:userId

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": {
    "id": 1,
    "email": "farhan@gmail.com",
    "username": "farhan",
    "age": 10,
    "password": "",
    "created": "2023-09-02T00:00:00Z"
  }
}
```

### POST /api/counter

#### Req

```json
{
  "type": 4,
  "kuota": 1,
  "price": 10000,
  "stock": 80
}
```

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": null
}
```

### GET /api/counter

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": [
    {
      "id": 1,
      "type": 5,
      "kuota": 10,
      "price": 10000,
      "stock": 9
    },
    {
      "id": 2,
      "type": 4,
      "kuota": 1,
      "price": 10000,
      "stock": 85
    }
  ]
}
```

### POST /api/counter/buy

#### Req

```json
{
    "user_id": 1,
    "list_paket_data_id":1
}
```

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": null
}
```

### PUT /api/counter/:listPaketId

#### Req

```json
{
  "type": 4,
  "kuota": 1,
  "price": 10000,
  "stock": 80
}
```

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": null
}
```

### GET /api/wallet/:walletId

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": {
    "id": 1,
    "saldo": 10000.0,
    "user_id": 1,
    "created": "2023-09-01T00:00:00Z"
  }
}
```

### PUT /api/wallet/:walletId

#### Req

```json
{
  "amount": 10.1
}
```

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": null
}
```

### GET /api/paket-data/:paketDataId

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": {
    "id": 1,
    "user_id": 1,
    "type": 0,
    "kuota": 0
  }
}
```

### DELETE /api/paket-data/:paketDataId

#### Res

```json
{
  "code": 200,
  "message": "success",
  "data": null
}
```
