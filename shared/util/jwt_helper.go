package util

import (
	"time"
	"voucher/config"
	"voucher/pkg/domain"

	"github.com/golang-jwt/jwt"
)

type JwtClaims struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	jwt.StandardClaims
}

func CreateJwtToken(user domain.User) (string, error) {
	conf := config.GetConfig()
	claims := JwtClaims{
		Id:       user.Id,
		Username: user.Username,
		Email:    user.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
		},
	}
	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	token, err := rawToken.SignedString([]byte(conf.SignKey))
	if err != nil {
		return "", err
	}
	return token, nil
}
