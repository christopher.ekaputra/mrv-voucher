CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    username VARCHAR(100) NOT NULL,
    age INT,
    password VARCHAR(1000) NOT NULL,
    created DATE
);

CREATE TABLE wallets (
    id BIGSERIAL PRIMARY KEY,
    saldo FLOAT DEFAULT 0,
    user_id INT UNIQUE,
    created DATE,
    CONSTRAINT fk_wallets_users
      FOREIGN KEY(user_id) 
      REFERENCES users(id)
      ON DELETE CASCADE
);

CREATE TABLE paket_data (
    id BIGSERIAL PRIMARY KEY,
    user_id INT UNIQUE,
    type INT DEFAULT 0,
    kuota INT DEFAULT 0,
    CONSTRAINT fk_paket_data_users
      FOREIGN KEY(user_id) 
      REFERENCES users(id)
      ON DELETE CASCADE
);

CREATE TABLE list_paket_data (
    id BIGSERIAL PRIMARY KEY,
    type INT,
    kuota INT,
    price FLOAT,
    stock INT
);

CREATE TABLE history_beli_paket_data (
    id BIGSERIAL PRIMARY KEY,
    list_paket_data_id INT,
    user_id INT,
    paket_data_id INT,
    wallet_id INT,
    created DATE,
    CONSTRAINT fk_history_beli_paket_data_users
      FOREIGN KEY(user_id) 
      REFERENCES users(id)
      ON DELETE CASCADE,
    CONSTRAINT fk_history_beli_paket_data_list_paket_data
      FOREIGN KEY(list_paket_data_id) 
      REFERENCES list_paket_data(id)
      ON DELETE CASCADE,
    CONSTRAINT fk_history_beli_paket_data_wallet
      FOREIGN KEY(wallet_id) 
      REFERENCES wallets(id)
      ON DELETE CASCADE,
    CONSTRAINT fk_history_beli_paket_data_paket_data
      FOREIGN KEY(paket_data_id) 
      REFERENCES paket_data(id)
      ON DELETE CASCADE
);