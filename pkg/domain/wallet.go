package domain

import "voucher/pkg/dto"

type Wallet struct {
	Id      int     `json:"id"`
	Saldo   float64 `json:"saldo"`
	UserId  int     `json:"user_id"`
	Created string  `json:"created"`
}

type WalletRepository interface {
	GetWallet(walletId int) (Wallet, error)
	CreateWallet(wallet Wallet) error
	UpdateWallet(walletId int, newAmount float64) error
}

type WalletUsecase interface {
	GetWallet(walletId int) (Wallet, error)
	UpdateSaldo(walletId int, req dto.UpdateSaldoRequest) error
}
