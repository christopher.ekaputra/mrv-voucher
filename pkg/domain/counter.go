package domain

import "voucher/pkg/dto"

type ListPaketData struct {
	Id    int     `json:"id"`
	Type  int     `json:"type"`
	Kuota int     `json:"kuota"`
	Price float64 `json:"price"`
	Stock int     `json:"stock"`
}

type HistoryBeliPaketData struct {
	Id              int    `json:"id"`
	ListPaketDataId int    `json:"list_paket_data_id"`
	UserId          int    `json:"user_id"`
	PaketDataId     int    `json:"paket_data_id"`
	WalletId        int    `json:"wallet_id"`
	Created         string `json:"created"`
}

type CounterRepository interface {
	GetListPaketData() ([]ListPaketData, error)
	GetListPaketDatabyId(id int) (ListPaketData, error)
	CreateListPaketData(req ListPaketData) error
	UpdateListPaketData(req ListPaketData) error
	CreateHistoryBeliPaketData(req HistoryBeliPaketData) error
}

type CounterUsecase interface {
	GetListPaketData() ([]ListPaketData, error)
	CreateListPaketData(req dto.ListPaketDataDTO) error
	UpdatePaketData(req dto.ListPaketDataDTO, listPaketDataId int) error
	BuyPaketData(req dto.BuyRequest) error
}
