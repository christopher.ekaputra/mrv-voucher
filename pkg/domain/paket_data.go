package domain

type PaketData struct {
	Id     int `json:"id"`
	UserId int `json:"user_id"`
	Type   int `json:"type"`
	Kuota  int `json:"kuota"`
}

type PaketDataRepository interface {
	GetPaketData(paketDataId int) (PaketData, error)
	CreatePaketData(paketData PaketData) error
	UpdatePaketData(paketData PaketData) error
	ResetPaketData(paketDataId int) error
}

type PaketDataUsecase interface {
	GetPaketData(paketDataId int) (PaketData, error)
	ResetPaketData(paketDataId int) error
}
