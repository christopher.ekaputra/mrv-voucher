package domain

import "voucher/pkg/dto"

type User struct {
	Id       int    `json:"id"`
	Email    string `json:"email"`
	Username string `json:"username"`
	Age      int    `json:"age"`
	Password string `json:"password"`
	Created  string `json:"created"`
}

type UserRepository interface {
	GetUser(id int) (User, error)
	CreateUser(req User) error
	GetUserByEmail(email string) (User, error)
}

type UserUsecase interface {
	GetUser(id int) (User, error)
	Register(req dto.UserDTO) error
	Login(req dto.LoginRequest) (interface{}, error)
}
