package usecase

import "voucher/pkg/domain"

type PaketDataUsecase struct {
	pdr domain.PaketDataRepository
}

func NewPaketDataUsecase(pdr domain.PaketDataRepository) domain.PaketDataUsecase {
	return &PaketDataUsecase{
		pdr: pdr,
	}
}

func (pdu *PaketDataUsecase) GetPaketData(paketDataId int) (domain.PaketData, error) {
	paketData, err := pdu.pdr.GetPaketData(paketDataId)
	if err != nil {
		return paketData, err
	}

	return paketData, nil
}

func (pdu *PaketDataUsecase) ResetPaketData(paketDataId int) error {
	_, err := pdu.pdr.GetPaketData(paketDataId)
	if err != nil {
		return err
	}

	err = pdu.pdr.ResetPaketData(paketDataId)
	if err != nil {
		return err
	}

	return nil
}
