package usecase

import (
	"errors"
	"voucher/pkg/domain"
	"voucher/pkg/dto"

	"github.com/mitchellh/mapstructure"
)

type CounterUsecase struct {
	CounterRepository   domain.CounterRepository
	WalletRepository    domain.WalletRepository
	PaketDataRepository domain.PaketDataRepository
}

func NewCounterUsecase(CR domain.CounterRepository, WR domain.WalletRepository, PDR domain.PaketDataRepository) domain.CounterUsecase {
	return &CounterUsecase{
		CounterRepository:   CR,
		WalletRepository:    WR,
		PaketDataRepository: PDR,
	}
}

func (cu CounterUsecase) GetListPaketData() ([]domain.ListPaketData, error) {
	return cu.CounterRepository.GetListPaketData()
}

func (cu CounterUsecase) CreateListPaketData(req dto.ListPaketDataDTO) error {
	var listPaketData domain.ListPaketData
	mapstructure.Decode(req, &listPaketData)
	return cu.CounterRepository.CreateListPaketData(listPaketData)
}

func (cu CounterUsecase) UpdatePaketData(req dto.ListPaketDataDTO, listPaketDataId int) error {
	err := req.Validation()
	if err != nil {
		return err
	}

	_, err = cu.CounterRepository.GetListPaketDatabyId(listPaketDataId)
	if err != nil {
		return errors.New("paket tidak ditemukan")
	}

	var listPaketData domain.ListPaketData
	mapstructure.Decode(req, &listPaketData)
	listPaketData.Id = listPaketDataId

	err = cu.CounterRepository.UpdateListPaketData(listPaketData)
	if err != nil {
		return errors.New("paket tidak dapat diupdate")
	}

	return nil
}

func (cu CounterUsecase) BuyPaketData(req dto.BuyRequest) error {
	paketBaru, err := cu.CounterRepository.GetListPaketDatabyId(req.ListPaketDataId)
	if err != nil {
		return errors.New("paket tidak ditemukan")
	}

	paketPrice := paketBaru.Price

	wallet, err := cu.WalletRepository.GetWallet(req.UserId)
	if err != nil {
		return errors.New("wallet tidak ditemukan")
	}
	walletSaldo := wallet.Saldo

	// 1. Cek saldo
	if walletSaldo < paketPrice {
		return errors.New("saldo tidak mencukupi")
	}

	// 1.a Cek type paket
	paketUser, err := cu.PaketDataRepository.GetPaketData(req.UserId)
	if err != nil {
		return errors.New("paket tidak ditemukan")
	}

	if paketUser.Type < paketBaru.Type && paketUser.Type != 0 {
		return errors.New("paket type terlalu tinggi")
	}

	if paketUser.Type == 0 {
		paketUser.Type = paketBaru.Type
	}

	// 2. Potong saldo
	newBalance := walletSaldo - paketPrice

	err = cu.WalletRepository.UpdateWallet(wallet.Id, newBalance)
	if err != nil {
		return err
	}

	// 3. Update paket stock pada counter
	paketBaru.Stock -= 1

	err = cu.CounterRepository.UpdateListPaketData(paketBaru)
	if err != nil {
		return errors.New("paket tidak dapat diupdate")
	}

	// 3.a Update paket pada user
	paketUser.Kuota += paketBaru.Kuota

	err = cu.PaketDataRepository.UpdatePaketData(paketUser)
	if err != nil {
		return errors.New("paket tidak dapat ditambahkan")
	}
	var newHistory domain.HistoryBeliPaketData
	newHistory.ListPaketDataId = paketBaru.Id
	newHistory.PaketDataId = paketUser.Id
	newHistory.UserId = req.UserId
	newHistory.WalletId = wallet.Id
	err = cu.CounterRepository.CreateHistoryBeliPaketData(newHistory)
	if err != nil {
		return err
	}

	return nil
}
