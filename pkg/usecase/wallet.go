package usecase

import (
	"voucher/pkg/domain"
	"voucher/pkg/dto"
)

type WalletUsecase struct {
	wr domain.WalletRepository
}

func NewWalletUsecase(wr domain.WalletRepository) domain.WalletUsecase {
	return &WalletUsecase{
		wr: wr,
	}
}

func (wu *WalletUsecase) GetWallet(walletId int) (domain.Wallet, error) {
	wallet, err := wu.wr.GetWallet(walletId)
	if err != nil {
		return wallet, err
	}
	return wallet, nil
}

func (wu *WalletUsecase) UpdateSaldo(walletId int, req dto.UpdateSaldoRequest) error {
	err := req.Validation()
	if err != nil {
		return err
	}

	wallet, err := wu.wr.GetWallet(walletId)
	if err != nil {
		return err
	}

	newSaldo := wallet.Saldo + req.Amount
	err = wu.wr.UpdateWallet(walletId, newSaldo)
	if err != nil {
		return err
	}

	return nil
}
