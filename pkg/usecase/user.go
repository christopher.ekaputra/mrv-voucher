package usecase

import (
	"errors"
	"voucher/pkg/domain"
	"voucher/pkg/dto"
	"voucher/shared/util"

	"github.com/mitchellh/mapstructure"
)

type UserUsecase struct {
	UserRepository      domain.UserRepository
	WalletRepository    domain.WalletRepository
	PaketDataRepository domain.PaketDataRepository
}

func NewUserUsecase(UR domain.UserRepository, WR domain.WalletRepository, PDR domain.PaketDataRepository) domain.UserUsecase {
	return &UserUsecase{
		UserRepository:      UR,
		WalletRepository:    WR,
		PaketDataRepository: PDR,
	}
}

func (uu UserUsecase) Register(req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)
	if _, err := uu.UserRepository.GetUserByEmail(user.Email); err == nil {
		return errors.New("email already exist")
	}
	// Hash password
	user.Password, _ = util.HashPassword(req.Password)
	err := uu.UserRepository.CreateUser(user)
	if err != nil {
		return errors.New("failed to create user")
	}
	user, err = uu.UserRepository.GetUserByEmail(req.Email)
	if err != nil {
		return errors.New("user not found")
	}

	var wallet domain.Wallet
	wallet.Saldo = 0
	wallet.UserId = user.Id

	err = uu.WalletRepository.CreateWallet(wallet)
	if err != nil {
		return errors.New("failed to create wallet")
	}

	var paketData domain.PaketData
	paketData.UserId = user.Id
	paketData.Type = 0
	paketData.Kuota = 0

	err = uu.PaketDataRepository.CreatePaketData(paketData)
	if err != nil {
		return errors.New("failed to create paket_data")
	}

	return nil
}

func (uu UserUsecase) GetUser(id int) (domain.User, error) {
	return uu.UserRepository.GetUser(id)
}

func (uu UserUsecase) Login(req dto.LoginRequest) (interface{}, error) {
	var loginResponse dto.LoginResponse
	user, err := uu.UserRepository.GetUserByEmail(req.Email)
	if err != nil {
		return nil, errors.New("email not found")
	}
	passwordValid := util.CheckPasswordHash(req.Password, user.Password)
	if !passwordValid {
		return nil, errors.New("bad credential")
	}
	token, err := util.CreateJwtToken(user)
	if err != nil {
		return nil, err
	}
	mapstructure.Decode(user, &loginResponse)
	loginResponse.Token = token

	return loginResponse, err

}
