package repository

import (
	"database/sql"
	"errors"
	"time"
	"voucher/pkg/domain"
)

type WalletRepository struct {
	db *sql.DB // nil
}

func NewWalletRepository(db *sql.DB) domain.WalletRepository {
	return &WalletRepository{
		db: db,
	}
}

func (wr *WalletRepository) GetWallet(walletId int) (domain.Wallet, error) {
	wallet := domain.Wallet{}
	sql := `SELECT id, saldo, user_id, created FROM Wallets WHERE id = $1`

	err := wr.db.QueryRow(sql, walletId).Scan(&wallet.Id, &wallet.Saldo, &wallet.UserId, &wallet.Created)
	if err != nil {
		return wallet, errors.New("wallet not found")
	}
	return wallet, nil
}

func (wr *WalletRepository) CreateWallet(wallet domain.Wallet) error {
	sql := `INSERT INTO wallets (saldo, user_id, created) values ($1, $2, $3)`
	stmt, err := wr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(wallet.Saldo, wallet.UserId, time.Now())
	if err2 != nil {
		return err2
	}
	return nil
}

func (wr *WalletRepository) UpdateWallet(walletId int, newAmount float64) error {
	sql := "UPDATE wallets set saldo = $1 where id = $2"

	_, err := wr.db.Exec(sql, newAmount, walletId)
	if err != nil {
		return err
	}

	return nil
}
