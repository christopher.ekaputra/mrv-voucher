package repository

import (
	"database/sql"
	"time"
	"voucher/pkg/domain"
)

type CounterRepository struct {
	db *sql.DB // nil
}

func NewCounterRepository(db *sql.DB) domain.CounterRepository {
	return &CounterRepository{
		db: db,
	}
}

func (cr CounterRepository) GetListPaketData() ([]domain.ListPaketData, error) {
	sql := `SELECT * FROM list_paket_data ORDER BY id ASC`
	rows, err := cr.db.Query(sql)
	var listPaketData []domain.ListPaketData
	for rows.Next() {
		var paketData domain.ListPaketData
		err2 := rows.Scan(&paketData.Id, &paketData.Type, &paketData.Kuota, &paketData.Price, &paketData.Stock)
		if err2 != nil {
			return nil, err2
		}
		listPaketData = append(listPaketData, paketData)
	}
	return listPaketData, err
}

func (cr CounterRepository) GetListPaketDatabyId(id int) (domain.ListPaketData, error) {
	var pd domain.ListPaketData
	sql := `SELECT id, type, kuota, price, stock FROM list_paket_data WHERE id = $1`

	err := cr.db.QueryRow(sql, id).Scan(&pd.Id, &pd.Type, &pd.Kuota, &pd.Price, &pd.Stock)
	return pd, err
}

func (cr CounterRepository) CreateListPaketData(req domain.ListPaketData) error {
	sql := `INSERT INTO list_paket_data (type, kuota, price, stock) values ($1, $2, $3, $4)`
	stmt, err := cr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Type, req.Kuota, req.Price, req.Stock)
	if err2 != nil {
		return err2
	}
	return nil
}

func (cr CounterRepository) UpdateListPaketData(listPaketData domain.ListPaketData) error {
	query := "UPDATE list_paket_data SET type = $1, kuota = $2, price = $3, stock = $4 WHERE id = $5"

	stmt, err := cr.db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(listPaketData.Type, listPaketData.Kuota, listPaketData.Price, listPaketData.Stock, listPaketData.Id)
	if err != nil {
		return err
	}

	return nil
}

func (cr CounterRepository) CreateHistoryBeliPaketData(req domain.HistoryBeliPaketData) error {
	sql := `INSERT INTO history_beli_paket_data (list_paket_data_id, user_id, paket_data_id, wallet_id, created) values ($1, $2, $3, $4, $5)`
	stmt, err := cr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.ListPaketDataId, req.UserId, req.PaketDataId, req.WalletId, time.Now())
	if err2 != nil {
		return err2
	}
	return nil
}
