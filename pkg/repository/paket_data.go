package repository

import (
	"database/sql"
	"errors"
	"voucher/pkg/domain"
)

type PaketDataRepository struct {
	db *sql.DB
}

func NewPaketDataRepository(db *sql.DB) domain.PaketDataRepository {
	return &PaketDataRepository{
		db: db,
	}
}

func (pdr *PaketDataRepository) GetPaketData(paketDataId int) (domain.PaketData, error) {
	paketData := domain.PaketData{}
	query := "SELECT id, user_id, type, kuota FROM paket_data WHERE id = $1"

	row := pdr.db.QueryRow(query, paketDataId)
	err := row.Scan(&paketData.Id, &paketData.UserId, &paketData.Type, &paketData.Kuota)
	if err != nil {
		return paketData, errors.New("paket data not found")
	}

	return paketData, nil
}

func (pdr *PaketDataRepository) CreatePaketData(paketData domain.PaketData) error {
	sql := `INSERT INTO paket_data (user_id, type, kuota) values ($1, $2, $3)`
	stmt, err := pdr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(paketData.UserId, paketData.Type, paketData.Kuota)
	if err2 != nil {
		return err2
	}
	return nil
}

func (pdr *PaketDataRepository) UpdatePaketData(paketData domain.PaketData) error {
	query := "UPDATE paket_data SET type = $1, kuota = $2 WHERE id = $3"

	_, err := pdr.db.Exec(query, paketData.Type, paketData.Kuota, paketData.Id)
	if err != nil {
		return err
	}

	return nil
}

func (pdr *PaketDataRepository) ResetPaketData(paketDataId int) error {
	query := "UPDATE paket_data SET type = 0, kuota = 0 WHERE id = $1"

	_, err := pdr.db.Exec(query, paketDataId)
	if err != nil {
		return err
	}

	return nil
}
