package repository

import (
	"database/sql"
	"time"
	"voucher/pkg/domain"
)

type UserRepository struct {
	db *sql.DB // nil
}

func NewUserRepository(db *sql.DB) domain.UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (sr UserRepository) GetUser(id int) (domain.User, error) {
	var user domain.User
	sql := `SELECT id, email, username, age, created FROM Users WHERE id = $1`

	err := sr.db.QueryRow(sql, id).Scan(&user.Id, &user.Email, &user.Username, &user.Age, &user.Created)
	return user, err
}

func (sr UserRepository) CreateUser(req domain.User) error {
	sql := `INSERT INTO Users (email, username, age, password, created) values ($1, $2, $3, $4, $5)`
	stmt, err := sr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Email, req.Username, req.Age, req.Password, time.Now())
	if err2 != nil {
		return err2
	}
	return nil
}

func (sr UserRepository) GetUserByEmail(email string) (domain.User, error) {
	var user domain.User
	sql := `SELECT * FROM Users WHERE email = $1`

	err := sr.db.QueryRow(sql, email).Scan(&user.Id, &user.Email, &user.Username, &user.Age, &user.Password, &user.Created)
	return user, err
}
