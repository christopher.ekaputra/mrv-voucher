package router

import (
	"database/sql"
	"voucher/pkg/controller"
	"voucher/pkg/repository"
	"voucher/pkg/usecase"

	"github.com/labstack/echo/v4"
)

func NewUserRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {

	ur := repository.NewUserRepository(db)
	wr := repository.NewWalletRepository(db)
	pdr := repository.NewPaketDataRepository(db)
	uu := usecase.NewUserUsecase(ur, wr, pdr)
	uc := &controller.UserController{
		UserUsecase: uu,
	}
	e.POST("/api/register", uc.Register)
	e.POST("/api/login", uc.Login)
	g.GET("/api/user/:id", uc.GetUser)
}
