package router

import (
	"database/sql"
	"voucher/pkg/controller"
	"voucher/pkg/repository"
	"voucher/pkg/usecase"

	"github.com/labstack/echo/v4"
)

func NewPaketDataRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	pdr := repository.NewPaketDataRepository(db)
	pdu := usecase.NewPaketDataUsecase(pdr)
	pdc := controller.PaketDataController{Pdu: pdu}

	g.GET("/api/paket-data/:paketDataId", pdc.GetPaketData)
	g.DELETE("/api/paket-data/:paketDataId", pdc.DeletePaketData)
}
