package router

import (
	"database/sql"
	"voucher/pkg/controller"
	"voucher/pkg/repository"
	"voucher/pkg/usecase"

	"github.com/labstack/echo/v4"
)

func NewWalletRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	wr := repository.NewWalletRepository(db)
	wu := usecase.NewWalletUsecase(wr)
	wc := controller.WalletController{Wu: wu}

	g.GET("/api/wallet/:walletId", wc.GetWallet)
	g.PUT("/api/wallet/:walletId", wc.PutWallet)
}
