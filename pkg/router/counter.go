package router

import (
	"database/sql"
	"voucher/pkg/controller"
	"voucher/pkg/repository"
	"voucher/pkg/usecase"

	"github.com/labstack/echo/v4"
)

func NewCounterRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	cr := repository.NewCounterRepository(db)
	wr := repository.NewWalletRepository(db)
	pdr := repository.NewPaketDataRepository(db)
	cu := usecase.NewCounterUsecase(cr, wr, pdr)
	cc := &controller.CounterController{
		CounterUsecase: cu,
	}

	e.GET("/api/counter", cc.GetListPaketData)
	g.POST("/api/counter", cc.CreateListPaketData)
	g.PUT("/api/counter/:listPaketId", cc.PutListPaketData)
	g.POST("/api/counter/buy", cc.Buy)
}
