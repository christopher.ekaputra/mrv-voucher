package dto

import validation "github.com/go-ozzo/ozzo-validation"

type ListPaketDataDTO struct {
	Type  int `json:"type"`
	Kuota int `json:"kuota"`
	Price int `json:"price"`
	Stock int `json:"stock"`
}

func (p ListPaketDataDTO) Validation() error {
	err := validation.ValidateStruct(&p,
		validation.Field(&p.Type, validation.Required),
		validation.Field(&p.Kuota, validation.Required),
		validation.Field(&p.Price, validation.Required),
		validation.Field(&p.Stock, validation.Required))

		if err != nil {
			return err
		}
		return nil
}