package dto

type BuyRequest struct {
	UserId          int `json:"user_id"`
	ListPaketDataId int `json:"list_paket_data_id"`
}
