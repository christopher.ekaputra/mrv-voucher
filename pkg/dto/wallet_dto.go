package dto

import validation "github.com/go-ozzo/ozzo-validation"

type UpdateSaldoRequest struct {
	Amount float64 `json:"amount"`
}

func (usr UpdateSaldoRequest) Validation() error {
	err := validation.ValidateStruct(&usr,
		validation.Field(&usr.Amount, validation.Required, validation.Min(float64(0.01))),
	)

	if err != nil {
		return err
	}
	return nil
}
