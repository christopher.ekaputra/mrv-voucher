package dto

type ChangePaketDataRequest struct {
	Id    int `json:"id"`
	Type  int `json:"type"`
	Kuota int `json:"kuota"`
}