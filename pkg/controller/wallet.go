package controller

import (
	"net/http"
	"strconv"
	"voucher/pkg/domain"
	"voucher/pkg/dto"
	"voucher/shared/util"

	"github.com/labstack/echo/v4"
)

type WalletController struct {
	Wu domain.WalletUsecase
}

func (wc WalletController) GetWallet(c echo.Context) error {
	walletId, err := strconv.Atoi(c.Param("walletId"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "wallet id in param must be number", nil)
	}

	wallet, err := wc.Wu.GetWallet(walletId)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success", wallet)
}

func (wc WalletController) PutWallet(c echo.Context) error {
	walletId, err := strconv.Atoi(c.Param("walletId"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "wallet id in param must be number", nil)
	}

	req := dto.UpdateSaldoRequest{}

	err = c.Bind(&req)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = wc.Wu.UpdateSaldo(walletId, req)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success", nil)
}
