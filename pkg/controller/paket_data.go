package controller

import (
	"net/http"
	"strconv"
	"voucher/pkg/domain"
	"voucher/shared/util"

	"github.com/labstack/echo/v4"
)

type PaketDataController struct {
	Pdu domain.PaketDataUsecase
}

func (pdc PaketDataController) GetPaketData(c echo.Context) error {
	paketDataId, err := strconv.Atoi(c.Param("paketDataId"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "paket data id in param must be number", nil)
	}

	paketData, err := pdc.Pdu.GetPaketData(paketDataId)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success", paketData)
}

func (pdc PaketDataController) DeletePaketData(c echo.Context) error {
	paketDataId, err := strconv.Atoi(c.Param("paketDataId"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "paket data id in param must be number", nil)
	}

	err = pdc.Pdu.ResetPaketData(paketDataId)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success", nil)
}
