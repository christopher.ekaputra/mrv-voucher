package controller

import (
	"net/http"
	"strconv"
	"voucher/pkg/domain"
	"voucher/pkg/dto"
	"voucher/shared/util"

	"github.com/labstack/echo/v4"
)

type CounterController struct {
	CounterUsecase domain.CounterUsecase
}

func (cc *CounterController) GetListPaketData(c echo.Context) error {
	res, err := cc.CounterUsecase.GetListPaketData()
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return util.SetResponse(c, http.StatusOK, "success", res)
}

func (cc *CounterController) CreateListPaketData(c echo.Context) error {
	var listPaketDataDTO dto.ListPaketDataDTO
	if err := c.Bind(&listPaketDataDTO); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}

	if err := listPaketDataDTO.Validation(); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	if err := cc.CounterUsecase.CreateListPaketData(listPaketDataDTO); err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return util.SetResponse(c, http.StatusOK, "success", nil)
}

func (cc *CounterController) PutListPaketData(c echo.Context) error {
	req := dto.ListPaketDataDTO{}

	err := c.Bind(&req)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	listPaketDataId, err := strconv.Atoi(c.Param("listPaketId"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = cc.CounterUsecase.UpdatePaketData(req, listPaketDataId)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "paket successfully updated", nil)
}

func (cc *CounterController) Buy(c echo.Context) error {
	var buyRequest dto.BuyRequest
	if err := c.Bind(&buyRequest); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	err := cc.CounterUsecase.BuyPaketData(buyRequest)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "paket successfully bought", nil)
}
