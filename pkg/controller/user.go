package controller

import (
	"net/http"
	"strconv"
	"voucher/pkg/domain"
	"voucher/pkg/dto"
	"voucher/shared/util"

	"github.com/labstack/echo/v4"
)

type UserController struct {
	UserUsecase domain.UserUsecase
}

func (uc *UserController) Register(c echo.Context) error {
	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userdto.Validation(); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	if err := uc.UserUsecase.Register(userdto); err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return util.SetResponse(c, http.StatusOK, "successfully registered a user", nil)
}

func (uc *UserController) Login(c echo.Context) error {
	var loginRequest dto.LoginRequest
	if err := c.Bind(&loginRequest); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	resp, err := uc.UserUsecase.Login(loginRequest)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserController) GetUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := uc.UserUsecase.GetUser(id)
	if err != nil {
		return util.SetResponse(c, http.StatusNotFound, "id user not found", nil)
	}
	return util.SetResponse(c, http.StatusOK, "success", resp)
}
